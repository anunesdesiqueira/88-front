export function DinamicSelect() {
  'ngInject';

  let directive = {
    restrict: 'E',
    scope: {
        extraValues: '=',
        label: "@label",
        class: "@"
    },
    templateUrl: './app/directives/templates/dinamic-select.html',
    link: linkFunc,
    controller: DinamicSelectController,
    controllerAs: 'vm'
  };

  return directive;

  // PARAMS: scope, el, attr, vm
  function linkFunc( scope ) {

    scope.itens = [];

    scope.toggle = function() {
      scope.new = !scope.new;
    }

    scope.setNew = function() {
      this.toggle();

      if( scope.newOption !== '' ) {
        scope.itens.push( scope.newOption );
        scope.newOption = '';
      }
    }

  }

}

class DinamicSelectController {
  constructor () {
    'ngInject';

  }
}
