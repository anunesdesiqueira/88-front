export function ConvertDate(Config) {
  'ngInject';

  let directive = {
    require: 'ngModel',

    link: function (scope, element, attrs, ngModelController) {
      // Convert from view to model
      ngModelController.$parsers.push(function (value) {

        if(value && moment(value).isValid())
          return moment(value).format( attrs.convertDate ? attrs.convertDate : null );

        return null;
      });

      // Convert from model to view
      ngModelController.$formatters.push(function (datetime) {
        if(datetime && moment(datetime).isValid())
          return moment(datetime).format(Config.date.short)

        return null;
      });
    }
  };

  return directive;
}
