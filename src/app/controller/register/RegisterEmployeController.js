export class RegisterEmployeController {
  constructor ($scope, Company, Departments, PositionsJobs, EmailSchema, Invitation, $state) {
    'ngInject';


    Departments.query({}, function (response) {
      $scope.deparments = response.data;
    });

    PositionsJobs.query({}, function (response) {
      $scope.positionsJobs = response.data;
    });

  }
}
