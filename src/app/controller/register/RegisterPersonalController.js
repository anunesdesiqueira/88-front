/*global FForm*/

export class RegisterPersonalController {
  constructor ( $scope, $document , Auth , UserProfile, AddressSchema, EmailSchema, PhoneSchema, $state, DocumentFiles, UserService ) {
    'ngInject';

    this.UserProfile = UserProfile;
    this.EmailSchema = EmailSchema;
    this.PhoneSchema = PhoneSchema;
    this.Auth = Auth;
    this.$scope = $scope;
    this.DocumentFiles = DocumentFiles;

    $scope.cropped = '';
    $scope.optionals = {
      identity_document : true,
      voter_id_card : true,
      driving_licence : true,
      work_record_booklet : true,
      social_integration_program : true,
      professional_identity : true
    }

    $scope.user = this.Auth.user();

    $scope.address_components = null;
    $scope.$watch ( 'address_components', function ( data_address) {
      if(typeof data_address == 'string')
        return;
      AddressSchema.set(data_address);
      var address = AddressSchema.getData();
      $scope.user_profile.address = address.address
      $scope.user_profile.data_address = [address.data_address];
    } );
    //check if has user profile
    if($scope.user.profile){
      $scope.user_profile = this.createNewProfile( $scope.user.profile );
    }else{
      $scope.user_profile = this.createNewProfile( { user_id : $scope.user.id } );
    }
    $scope.user_profile.document.setUrl('/medias');
    $scope.user_profile.document.extend({
      slug : 'user_avatar',
      user_id: UserService.userId(),
      reference: UserService.userId()
    });
    $scope.address_components = $scope.user_profile.address;


    $scope.sending = false;
    var formWrap = $document[0].getElementById('fs-form-wrap');
    new FForm(formWrap, {
      onReview: function() {
        // Ação depois do ultimo item do form
        var btnCallback = angular.element(document.querySelector('.fs-next-register'));
        console.log( btnCallback.text() );

        if( btnCallback.text() === '' ) {
          btnCallback.hide();
        }
      },
      textCallback: "",
      onFinish: function(){ $scope.saveOrUpdate(); },
      onCallback: function(){ $scope.saveOrUpdate(); }
    });

    $scope.setDotError = function( index ) {
      var navDots = angular.element(document.querySelector('.fs-nav-dots'))
        , dots = navDots.children();

      dots.eq(index).addClass('fs-dot-error');
    }

    $scope.getStepError = function( keys ) {
      keys.forEach(function(currKey){
        var key = currKey.split('.').shift()
          , step = angular.element(document.getElementById(key));

        $scope.setDotError(step.index());
      });
    }

    $scope.clearErrors = function() {
      var dots = angular.element(document.querySelector('.fs-nav-dots')).children();
      dots.removeClass('fs-dot-error');
    }

    $scope.error = function( response ) {
      var keys = _.allKeys(response.data.errors);
      $scope.clearErrors();
      $scope.getStepError(keys);
    }

    $scope.saveOrUpdate = function () {
      if($scope.user_profile.id){
        if($scope.user_profile.document.file){
          $scope.user_profile.document.save().then(function(response){
            Auth.setAvatar(response.data);
            $scope.user_profile.file_id = response.data.id;
            $scope.user_profile.$update($scope.success, $scope.error);
          }, $scope.error);
        }else{
          $scope.user_profile.$update($scope.success, $scope.error);
        }
      }else{
        if($scope.user_profile.document.file){
          $scope.user_profile.document.save().then(function(response){
            Auth.setAvatar(response.data);
            $scope.user_profile.file_id = response.data.id;
            $scope.user_profile.$save($scope.success, $scope.error);
          }, $scope.error);
        }else{
          $scope.user_profile.$save($scope.success, $scope.error);
        }
      }
    }

    $scope.success = function (response) {
      //update $local
      Auth.setProfile(response.data);
      $state.go('start', {}, {
        notify: true,
        reload: true
      });
    }


    $scope.$watch("user_profile.data_phone_emergency", function () {
      var last = _.last($scope.user_profile.data_phone_emergency);
      if(last.phone){
        var item = PhoneSchema.item();
        $scope.user_profile.data_phone_emergency.push(item);
      }
    },true)

    $scope.$watch("user_profile.name", function (name) {
      if(name)
        $scope.user_profile.firstName = name.split(' ').shift()
    },true)
  }
  createNewProfile( data ) {
    var _this = this;
    return new this.UserProfile( angular.extend({
      user_id: null,
      data_address : null,
      data_email: this.EmailSchema.start({main: true}),
      data_phone: this.PhoneSchema.start({main: true},2),
      data_phone_emergency: this.PhoneSchema.start({main: true},2),
      name : null,
      firstName : null,
      gender : null,
      date_of_birth : null,
      identity_document : null,
      identity_document_state : null,
      identity_document_dispatcher_organ : null,
      identity_document_dispatcher_date : null,
      register_individual : null,
      voter_id_card : null,
      driving_licence : null,
      driving_licence_category : null,
      driving_licence_dispatcher_date : null,
      driving_licence_validate_date : null,
      driving_licence_nope : _this.$scope.optionals.driving_licence ? true : null,
      work_record_booklet : null,
      work_record_booklet_series : null,
      work_record_booklet_dispatcher_state : null,
      work_record_booklet_nope : _this.$scope.optionals.work_record_booklet ? true : null,
      social_integration_program : null,
      social_integration_program_nope : _this.$scope.optionals.social_integration_program ? true : null,
      professional_identity : null,
      professional_identity_nope : _this.$scope.optionals.professional_identity ? true : null,
      about_me : null,
      file_id : null,
      document : this.DocumentFiles
    }, data ));
  }
}
