/*global FForm*/

export class RegisterCompanyController {
  constructor ( $scope, $document , Company, Auth , Sectors, TaxFrameworks, AddressSchema, EmailSchema, PhoneSchema, $state ) {
    'ngInject';

    $scope.reviewForm = false;
    $scope.optionals = {
      state_registration : true,
      tax_framework_id : true
    }

    $scope.company = new Company({
      data_address : null,
      data_email: EmailSchema.start({main: true}),
      data_phone: PhoneSchema.start({main: true}),
      type : null,
      trading_name : null,
      company_name : null,
      national_register_of_legal_entities : null,
      state_registration : null,
      sector_id : null,
      tax_framework_id : null,
      //optionals
      state_registration_exempted : $scope.optionals.state_registration ? $scope.optionals.state_registration : false,
      tax_framework_nope : $scope.optionals.tax_framework_id ? $scope.optionals.tax_framework_id : false,//@todo change key
    });

    $scope.address_components = null;

    $scope.$watch ( 'company.national_register_of_legal_entities', function ( cnpj ) {//makes a validation before go next
      $scope.company.national_register_of_legal_entities_valid = $scope.company.national_register_of_legal_entities ? $scope.company.national_register_of_legal_entities : '';
      console.log($scope.company.national_register_of_legal_entities_valid);
    } );

    $scope.$watch ( 'address_components', function ( data_address) {
      AddressSchema.set(data_address);
      var address = AddressSchema.getData();
      $scope.company.address = address.address
      $scope.company.data_address = [address.data_address];
    } );
    $scope.user = Auth.user();

    Sectors.query({}, function (response) {
      $scope.sectors = response.data
    })

    TaxFrameworks.query({}, function (response) {
      $scope.taxes = response.data
    })

    $scope.onNextStep = function (form) {
      console.log()
      $scope.isOnLastStep = ((form.current + 1) == form.fieldsCount) ? true : false;
      console.log("isOnLastStep", $scope.isOnLastStep);
    }

    var formWrap = $document[0].getElementById('fs-form-wrap');
    window.formItem = $scope.formItem = new FForm(formWrap, {
      onNextStep: $scope.onNextStep,
      onReview: function() {
        // Ação depois do ultimo item do form
        var btnCallback = angular.element(document.querySelector('.fs-next-register'));
        console.log( btnCallback.text() );

        if( btnCallback.text() === '' ) {
          btnCallback.hide();
        }
      },

      textCallback: 'Adicionar filial',
      onFinish: function(){ console.log('finish'); $scope.save(); },
      onCallback: function(){ console.log('Callback') }
    });

    $scope.setDotError = function( index ) {
      var navDots = angular.element(document.querySelector('.fs-nav-dots'))
        , dots = navDots.children();

      dots.eq(index).addClass('fs-dot-error');
    }

    $scope.getStepError = function( keys ) {
      keys.forEach(function(currKey){
        var key = currKey.split('.').shift()
          , step = angular.element(document.getElementById(key));

        $scope.setDotError(step.index());
      });
    }

    $scope.clearErrors = function() {
      var dots = angular.element(document.querySelector('.fs-nav-dots')).children();
      dots.removeClass('fs-dot-error');
    }

    $scope.error = function( response ) {
      var keys = _.allKeys(response.data.errors);
      $scope.clearErrors();
      $scope.getStepError(keys);
    }

    $scope.save = function () {
      $scope.company.$save(function (response) {
        $state.go('register.employe', {}, {
          notify: true,
          reload: true
        });
      }, $scope.error);
    }

    $scope.myImage='';
    $scope.myCroppedImage='';
    var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $scope.myImage=evt.target.result;
        });
      };
      reader.readAsDataURL(file);
    };

    var input = $document[0].querySelector('#fileInput');

    angular.element( input ).on('change', handleFileSelect);

  }
}
