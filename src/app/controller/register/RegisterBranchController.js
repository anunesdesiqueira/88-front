/*global FForm*/

export class RegisterBranchController {
  constructor ( $document, $scope ) {
    'ngInject';

    var formWrap = $document[0].getElementById( 'fs-form-wrap' );
    new FForm( formWrap, {
      onReview: function() {
        // Ação depois do ultimo item do form
        var btnCallback = angular.element(document.querySelector('.fs-next-register'));
        console.log( btnCallback.text() );

        if( btnCallback.text() === '' ) {
          btnCallback.hide();
        }
      }
    });

  }
}
