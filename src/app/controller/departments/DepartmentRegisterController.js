/**
 * Created by robin on 22/09/16.
 */
export class DepartmentRegisterController{
    constructor ($scope, Departments, Auth) {
        'ngInject';

        $scope.title = "Cadastro de departamentos";

        $scope.saveOrUpdate = function () {
            if(!$scope.department.$resolved){
                return false;//is resolving first click;
            }
            $scope.department.$resolved = false;
            if($scope.department.id){
                $scope.department.$update($scope.success)
            }else{
                $scope.department.$save($scope.success)
            }
        }

        $scope.success = function (response) {
            delete $scope.department;
            $scope.department = $scope.newDepartment({});
            $scope.params.time = moment().format();
            $scope.$emit("resourceDepartmentSuccess", response );
        }

        $scope.newDepartment = function (data) {
            return new Departments(angular.extend({
                company_id: Auth.getCurrentCompany().id,
                $resolved : true
            },data));
        }

        $scope.department = $scope.newDepartment({});
    }

}
