/**
 * Created by robin on 07/09/16.
 */
export class LicensesIndexController{
    constructor ($scope, License, User, Config, $stateParams , Auth, LicensesStatus) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;
        //define filters
        $scope.searchParams = {
            name: null,
            user_id: null,
            status: null
        }
        $scope.params = angular.copy($scope.searchParams);
        //watch for search event
        $scope.$on('searchChanged', function (event,data,params) {
            if(params)
                $scope.params = params;

            $scope.load();
        }, true );

        $scope.pagination = angular.extend( Config.pagination , { current_page: $stateParams.page ? $stateParams.page : 1 });
        $scope.list = [];

        $scope.statuses = LicensesStatus.all();

        User.query(_this.newQuery(), function (response) {
            $scope.users = response.data;
        });

        $scope.load = function () {
            delete $scope.resourceItem;
            $scope.resourceItem = License.query( angular.extend( $scope.pagination , _this.newQuery(), $scope.params ) , function (response) {
                $scope.list = response.data;
                $scope.pagination = response.meta.pagination;
            });
        }

        $scope.setStatus = function (item) {
            item.status_name = _.findWhere( $scope.statuses , { slug : item.status });
        }

        $scope.saveStatus = function (item) {
            item.$resolved = false;//show loading
            License.update( {} , item, function (response) {
                item.$resolved = true;
            },function (error) {
                item.$resolved = true;
            } )
        }

        $scope.paginateTo = function (newPageNumber) {
            $scope.pagination.page = newPageNumber;
            $scope.load();
        }

        $scope.getToEdit = function (item) {
            $scope.currentItemToEdit = item;
        }

    }
    newQuery(){
        return {
            include : 'user',
            company_id : this.Auth.getCurrentCompanyId()
        }
    }
}
