/**
 * Created by robin on 04/09/16.
 */
export class LicensesRegisterController{
    constructor ($scope, License , User, Auth, $uibModal, LicensesStatus) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;

        $scope.title = "Cadastro licença";

        $scope.loadDependencies = function () {
            $scope.statuses = LicensesStatus.all();

            User.query({}, function (response) {
                $scope.users = response.data;
            });
        }

        $scope.saveOrUpdate = function () {
            if(!$scope.license.$resolved){
                return false;//is resolving first click;
            }
            $scope.license.$resolved = false;

            //format name
            if($scope.license.user){
                $scope.license.name = $scope.license.user.display_name;
                $scope.license.user_id = $scope.license.user.id;
            }
            if($scope.license.id){
                $scope.license.$update($scope.success)
            }else{
                $scope.license.$save($scope.success)
            }
        }

        $scope.success = function (response) {
            delete $scope.license;
            $scope.license = $scope.newLicense({});
            $scope.params.time = moment().format();
            $scope.$emit("searchChanged");
        }

        $scope.newLicense = function (data) {
            return new License(angular.extend({
                starting : null,
                ending : null,
                one_day: false,
                company_id: Auth.getCurrentCompany().id,
                $resolved : true
            },data));
        }

        $scope.$watch("currentItemToEdit", function (data) {
            if(data){
                $scope.license = $scope.newLicense(data);
                $scope.license.date = moment($scope.license.date).format("DD/MM/YYYY");

            }

        })
        $scope.loadDependencies();
        $scope.license = $scope.newLicense({});
        $scope.modal = null;

        $scope.setOneDay = function () {
            if($scope.license.one_day){
                $scope.license.ending = angular.copy($scope.license.starting);
            }else{
                $scope.license.ending = null;
            }
        }
    }
    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }


}
