/**
 * Created by robin on 04/09/16.
 */
export class VacationsRegisterController{
    constructor ($scope, Vacation , User, Auth, $uibModal, VacationsStatus) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;

        $scope.title = "Cadastro de férias funcionário";

        $scope.loadDependencies = function () {
            $scope.statuses = VacationsStatus.all();

            User.query({}, function (response) {
                $scope.users = response.data;
            });
        }

        $scope.saveOrUpdate = function () {
            if(!$scope.vacation.$resolved){
                return false;//is resolving first click;
            }
            $scope.vacation.$resolved = false;

            //format name
            if($scope.vacation.user){
                $scope.vacation.name = $scope.vacation.user.display_name;
                $scope.vacation.user_id = $scope.vacation.user.id;
            }
            if($scope.vacation.id){
                $scope.vacation.$update($scope.success)
            }else{
                $scope.vacation.$save($scope.success)
            }
        }

        $scope.success = function (response) {
            delete $scope.vacation;
            $scope.vacation = $scope.newVacation({});
            $scope.params.time = moment().format();
            $scope.$emit("searchChanged");
        }

        $scope.newVacation = function (data) {
            return new Vacation(angular.extend({
                type: 'user',
                company_id: Auth.getCurrentCompany().id,
                $resolved : true
            },data));
        }

        $scope.$watch("currentItemToEdit", function (data) {
            if(data){
                $scope.vacation = $scope.newVacation(data);
                $scope.vacation.date = moment($scope.vacation.date).format("DD/MM/YYYY");

            }

        })
        $scope.loadDependencies();
        $scope.vacation = $scope.newVacation({});
        $scope.modal = null;
    }
    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }


}
