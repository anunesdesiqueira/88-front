/**
 * Created by robin on 07/09/16.
 */
export class VacationsCollectiveRegisterController{
    constructor ($scope, Vacation , User, Auth, $uibModal, VacationsStatus, Departments) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;

        $scope.title = "Cadastro férias coletiva";

        $scope.loadDependencies = function () {
            $scope.statuses = VacationsStatus.all();

            User.query({}, function (response) {
                $scope.users = response.data;
            });

            Departments.query( _this.newQuery() , function (response) {
                $scope.departments = response.data;
                $scope.departments.unshift($scope.vacation.department)
            });
        }

        $scope.saveOrUpdate = function () {
            if(!$scope.vacation.$resolved){
                return false;//is resolving first click;
            }
            $scope.vacation.$resolved = false;

            //format name
            $scope.vacation.name = $scope.vacation.department.name;
            if($scope.vacation.department.id != 'all'){
                $scope.vacation.type = 'department';
                $scope.vacation.department_id = $scope.vacation.department.id;
            }else{
                $scope.vacation.type = 'company';
            }
            if($scope.vacation.id){
                $scope.vacation.$update($scope.success)
            }else{
                $scope.vacation.$save($scope.success)
            }
        }

        $scope.success = function (response) {
            delete $scope.vacation;
            $scope.vacation = $scope.newVacation({});
            $scope.params.time = moment().format();
            $scope.$emit("searchChanged");
        }

        $scope.newVacation = function (data) {
            return new Vacation(angular.extend({
                department: {
                    id: 'all',
                    name: 'Todos'
                },
                status: 'approved',
                company_id: Auth.getCurrentCompany().id,
                $resolved : true
            },data));
        }

        $scope.$watch("currentItemToEdit", function (data) {
            if(data){
                $scope.vacation = $scope.newVacation(data);
                $scope.vacation.date = moment($scope.vacation.date).format("DD/MM/YYYY");

            }

        })
        $scope.loadDependencies();
        $scope.vacation = $scope.newVacation({});
        $scope.modal = null;
    }
    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }


}
