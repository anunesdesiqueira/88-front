/**
 * Created by robin on 03/09/16.
 */
export class ProvidersIndexController{
    constructor ($scope, Provider, ProviderCategory, Config, $stateParams , Auth) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;
        //define filters
        $scope.searchParams = {
            company_id : Auth.getCurrentCompanyId(),
            name: null,
            provider_category_id : null
        }
        $scope.params = angular.copy($scope.searchParams);
        //watch for search event
        $scope.$on('searchChanged', function (event,data,params) {
            if(params)
                $scope.params = params;

            $scope.loadProviders();
        }, true );

        $scope.pagination = angular.extend( Config.pagination , { current_page: $stateParams.page ? $stateParams.page : 1 });
        $scope.providers = [];

        ProviderCategory.query({}, function (response) {
            $scope.categories = response.data;
        });

        $scope.loadProviders = function (params) {
            delete $scope.resourceProvider;
            $scope.resourceProvider = Provider.query( angular.extend( $scope.pagination , _this.newQuery(), $scope.params ) , function (response) {
                $scope.providers = response.data;
                $scope.pagination = response.meta.pagination;
            });
        }

        $scope.setCategory = function (item) {
            item.category = _.findWhere( $scope.categories , { id : item.provider_category_id });
        }

        $scope.paginateTo = function (newPageNumber) {
            $scope.pagination.page = newPageNumber;
            $scope.loadProviders();
        }

        $scope.getToEdit = function (item) {
            $scope.currentItemToEdit = item;
        }

    }
    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }
}
