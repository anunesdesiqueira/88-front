/**
 * Created by robin on 03/09/16.
 */
export class ProvidersRegisterController{
    constructor ($scope, Provider, ProviderCategory, EmailSchema, PhoneSchema, AddressSchema, Auth, $stateParams) {
        'ngInject';

        $scope.title = "Cadastro de fornecedores";
        $scope.address_components = null;


        $scope.$watch ( 'address_components', function ( data_address ) {
            if(typeof data_address == 'string')
                return;
            AddressSchema.set(data_address);
            var address = AddressSchema.getData();
            $scope.provider.address = address.address
            $scope.provider.data_address = [address.data_address];
        } );

        $scope.types = [];
        ProviderCategory.query({}, function (response) {
            $scope.categories = response.data;
        });

        $scope.saveOrUpdate = function () {
            if(!$scope.provider.$resolved){
                return false;//is resolving first click;
            }
            $scope.provider.$resolved = false;
            if($scope.provider.id){
                $scope.provider.$update($scope.success)
            }else{
                $scope.provider.$save($scope.success)
            }
        }

        $scope.success = function (response) {
            delete $scope.provider;
            $scope.provider = $scope.newProvider({});
            $scope.params.time = moment().format();
            $scope.$emit("searchChanged");
            $scope.$emit("resourceProviderSuccess", response );
        }

        $scope.newProvider = function (data) {
            var Email = EmailSchema;
            var Phone = PhoneSchema;
            $scope.address_components = data.address ? data.address : null;
            return new Provider(angular.extend({
                data_address : null,
                data_email: Email.start({main: true}),
                data_phone: Phone.start({main: true}),
                company_id: Auth.getCurrentCompany().id,
                $resolved : true
            },data));
        }

        $scope.$watch("currentItemToEdit", function (data) {
            if(data)
                $scope.provider = $scope.newProvider(data);

        })
        $scope.provider = $scope.newProvider({});
    }

}
