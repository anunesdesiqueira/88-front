/**
 * Created by robin on 04/09/16.
 */
export class RefundsRegisterController{
    constructor ($scope, Refund , User, TypeExpenditure, Auth, $uibModal, RefundsStatus) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;

        $scope.title = "Cadastro de reembolso";

        $scope.loadDependencies = function () {
            $scope.statuses = RefundsStatus.all();
            TypeExpenditure.query({}, function (response) {
                $scope.categories = response.data;
            });
            User.query({}, function (response) {
                $scope.users = response.data;
            });
        }

        $scope.saveOrUpdate = function () {
            if(!$scope.refund.$resolved){
                return false;//is resolving first click;
            }
            $scope.refund.$resolved = false;
            if($scope.refund.user){
                $scope.refund.name = $scope.refund.user.display_name;
                $scope.refund.user_id = $scope.refund.user.id;
            }
            if($scope.refund.id){
                $scope.refund.$update($scope.success)
            }else{
                $scope.refund.$save($scope.success)
            }
        }

        $scope.success = function (response) {
            delete $scope.refund;
            $scope.refund = $scope.newRefund({});
            $scope.params.time = moment().format();
            $scope.$emit("searchChanged");
        }

        $scope.newRefund = function (data) {
            return new Refund(angular.extend({
                company_id: Auth.getCurrentCompany().id,
                $resolved : true
            },data));
        }

        $scope.$watch("currentItemToEdit", function (data) {
            if(data){
                $scope.refund = $scope.newRefund(data);
                $scope.refund.date = moment($scope.refund.date).format("DD/MM/YYYY");

            }

        })
        $scope.loadDependencies();
        $scope.refund = $scope.newRefund({});
        $scope.modal = null;
    }
    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }


}
