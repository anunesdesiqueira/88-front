/**
 * Created by robin on 04/09/16.
 */
export class BenefitsIndexController{
    constructor ($scope, Benefit, Provider, BenefitCategory, Config, $stateParams , Auth) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;
        //define filters
        $scope.searchParams = {
            name: null,
            benefit_category_id : null,
            provider_id: null
        }
        $scope.params = angular.copy($scope.searchParams);
        //watch for search event
        $scope.$on('searchChanged', function (event,data,params) {
            if(params)
                $scope.params = params;

            $scope.load();
        }, true );

        $scope.pagination = angular.extend( Config.pagination , { current_page: $stateParams.page ? $stateParams.page : 1 });
        $scope.list = [];

        BenefitCategory.query({}, function (response) {
            $scope.categories = response.data;
        });

        Provider.query( _this.newQuery() , function (response) {
            $scope.providers = response.data;
        });

        $scope.load = function () {
            delete $scope.resourceItem;
            $scope.resourceItem = Benefit.query( angular.extend( $scope.pagination , _this.newQuery(), $scope.params ) , function (response) {
                $scope.list = response.data;
                $scope.pagination = response.meta.pagination;
            });
        }

        $scope.setCategory = function (item) {
            item.category = _.findWhere( $scope.categories , { id : item.benefit_category_id });
        }
        $scope.setProvider = function (item) {
            item.provider = _.findWhere( $scope.providers , { id : item.provider_id });
        }

        $scope.paginateTo = function (newPageNumber) {
            $scope.pagination.page = newPageNumber;
            $scope.load();
        }

        $scope.getToEdit = function (item) {
            $scope.currentItemToEdit = item;
        }

    }
    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }
}
