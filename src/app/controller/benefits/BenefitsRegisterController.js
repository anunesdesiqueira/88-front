/**
 * Created by robin on 04/09/16.
 */
export class BenefitsRegisterController{
    constructor ($scope, Benefit , Provider, BenefitCategory, Auth, Company, Departments, Team, $uibModal) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;

        $scope.title = "Cadastro de Benefícios";
        $scope.title2 = "Cadastro de beneficiários";

        $scope.types = [];

        $scope.loadDependencies = function () {
            BenefitCategory.query( _this.newQuery() , function (response) {
                $scope.categories = response.data;
            });

            Provider.query( _this.newQuery() , function (response) {
                $scope.providers = response.data;
            });

            Company.query( _this.newQuery() , function (response) {
                $scope.companies = response.data;
            })

            Departments.query( _this.newQuery() , function (response) {
                $scope.departments = response.data;
            });

            Team.query( _this.newQuery() , function (response) {
                $scope.users = response.data;
            });
        }

        $scope.saveOrUpdate = function () {
            if(!$scope.benefit.$resolved){
                return false;//is resolving first click;
            }
            $scope.benefit.$resolved = false;
            if($scope.benefit.id){
                $scope.benefit.$update($scope.success)
            }else{
                $scope.benefit.$save($scope.success)
            }
        }

        $scope.success = function (response) {
            delete $scope.benefit;
            $scope.benefit = $scope.newBenefit({});
            $scope.params.time = moment().format();
            $scope.$emit("searchChanged");
        }

        $scope.newBenefit = function (data) {
            return new Benefit(angular.extend({
                company_id: Auth.getCurrentCompany().id,
                $resolved : true
            },data));
        }

        $scope.$watch("currentItemToEdit", function (data) {
            if(data)
                $scope.benefit = $scope.newBenefit(data);

        })
        $scope.loadDependencies();
        $scope.benefit = $scope.newBenefit({});
        $scope.modal = null;
        $scope.addProvider = function() {
            $scope.modal = $uibModal.open({
                size: 'xs',
                templateUrl: 'app/view/providers/register.html',
                scope: $scope
            });
            $scope.modal.result.then(function (response) {
                $scope.benefit.provider_id = response.id;
                $scope.loadDependencies();
            })
        }

        $scope.$on("resourceProviderSuccess", function (event, data) {
            $scope.modal.close(data.data);
        })
    }

    newQuery() {
        return {
            company_id: this.Auth.getCurrentCompanyId()
        }
    }


}
