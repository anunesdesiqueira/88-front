/**
 * Created by robin on 03/09/16.
 */
export class AppLogoutController {
    constructor ($state, Auth) {
        'ngInject';

        Auth.logout().then( function () {
            Auth.clear();
            $state.go('auth.login', { }, {
                notify: true,
                reload: true
            });
        })

    }
}
