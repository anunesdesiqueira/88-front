/**
 * Created by robin on 03/09/16.
 */
export class StartController {
    constructor (currentCompany,$state) {
        'ngInject';
        //redirect to start url
        $state.go('app.index', { company_url: currentCompany.slug }, {
            notify: true,
            reload: true
        });
    }
}
