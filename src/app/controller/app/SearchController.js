/**
 * Created by robin on 04/09/16.
 */
export class SearchController {
    constructor ( $scope ) {
        'ngInject';

        var _this = this;
        _this.$scope = $scope;
        $scope.isActive = false;
        $scope.search = _this.newSearch();

        $scope.resetSearch = function () {
            $scope.search = _this.newSearch();
            $scope.SearchForm.$setPristine();
        }

        $scope.$watch('search', function (data) {
            $scope.$emit("searchChanged", data , _this.getSearchParam(data))
            $scope.isActive = $scope.SearchForm.$dirty;
        }, true );

    }

    newSearch(){
        return angular.extend( this.getParams() , {
            time: moment().format()
        })
    }

    getParams(){
        return this.$scope.searchParams ? angular.copy(this.$scope.searchParams) : {} ;
    }

    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }
    getSearchParam(search){
        var params = [];
        angular.forEach( search , function (value,param) {
            if(param != 'time'){
                if(value){
                    params.push(param + ":" + value);
                }
            }
        });
        return { search : params.join(";") };
    }
}
