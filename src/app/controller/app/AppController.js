/**
 * Created by robin on 02/09/16.
 */
export class AppController {
    constructor ($scope, UserService, $rootScope) {
        'ngInject';
        $scope.UserService = UserService;
        $scope.user = UserService.user();

        $rootScope.$on("hideHeader", function (event) {
            $scope.hideHeader = $rootScope.hideHeader;
        });

    }
}
