export class TableController {
  constructor( $scope ) {
    'ngInject';

    $scope.expanded = true;

    $scope.expandRow = function() {
      $scope.expanded = !$scope.expanded;
    }
  }
}
