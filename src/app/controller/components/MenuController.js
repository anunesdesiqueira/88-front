export class MenuController {
  constructor( $scope ) {
    'ngInject';

     $scope.clean = function() {
      var activeMenu = document.querySelector('.submenu--active');
      angular.element(activeMenu).removeClass('submenu--active');
     }

    $scope.showSubmenu = function(id) {
      $scope.clean();
      var menu = angular.element(document.getElementById(id));
      menu.toggleClass('submenu--active');
    }

  }
}
