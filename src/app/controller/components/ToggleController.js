export class ToggleController {
  constructor( $scope ) {
    'ngInject';

    $scope.componentActive = false;

    $scope.options = [
      '-- Selecione --',
      'São Paulo',
      'Rio de Janeiro',
      'Mato Grosso do Sul'
    ];

    $scope.showItens = function() {
      $scope.componentActive = !$scope.componentActive;
    }
  }
}
