export class SelectController {
  constructor( $scope ) {
    'ngInject';

    $scope.componentActive = false;
    $scope.label = 'Selecione...';
    $scope.current = false;

    console.log($scope.model);
    $scope.$parent.$on( $scope.model, function (data) {
      $scope.options = data;
    })

    $scope.showItens = function() {
      $scope.componentActive = !$scope.componentActive;
    }

    $scope.change = function (option) {
      $scope.$emit($scope.callback, option);
      $scope.current = option;
    }

    $scope.reset = function () {
      $scope.$emit($scope.callback, false);
      $scope.current = false;
    }
  }
}
