export class TabController {
  constructor( $scope, $log ) {
    'ngInject';

    $scope.getTab = function($event) {
      return angular.element($event.currentTarget).parent().parent();
    }

    $scope.clean = function($event) {
      var tab = $scope.getTab($event);
      tab.find('.tab__section').removeClass('tab__section--active');
      tab.find('.tab__btn').removeClass('tab__btn--active');
    }

    $scope.activeTab = function($event){
      angular.element($event.currentTarget).addClass('tab__btn--active');
    }

    $scope.showContent = function($event, id){
      var tab = $scope.getTab($event);
      tab.find('.tab__section--' + id).addClass('tab__section--active');
    }

    $scope.initTab = function($event, id) {
      $scope.clean($event);
      $scope.activeTab($event);
      $scope.showContent($event, id);
    }

  }
}
