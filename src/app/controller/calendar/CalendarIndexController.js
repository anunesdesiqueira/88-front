/**
 * Created by robin on 07/09/16.
 */
export class CalendarIndexController{
    constructor ($scope, Team, $stateParams, Config, Auth) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;
        $scope.eventSources = [
            {title: 'All Day Event',start: moment().add(1,"day").toDate()}
        ];
        $scope.uiConfig = {
            calendar:{
                //height: '450px',
                //editable: true,
                header:{
                    left: 'Calendário',
                    center: 'title',
                    right: 'today prev,next'
                },
                viewRender: function(view, element) {
                    $scope.load({
                        start : view.start.format(),
                        end : view.end.format()
                    })
                }
            }
        }

        $scope.list = [];

        $scope.load = function (params) {
            console.log( params);
            delete $scope.resourceItem;
            $scope.resourceItem = Team.query( angular.extend( _this.newQuery(), params ) , function (response) {
                $scope.list = response.data;
                $scope.pagination = response.meta.pagination;
            });
        }

        $scope.$watch("list" , function (data) {
            $scope.eventSources = _.filter(data,  function (item) {
                if(!item.profile){
                    return false;
                }
                if(!item.profile.data.date_of_birth){
                    return false;
                }

                return true;

            }).map(function (item) {
                return {
                    title: 'Aniversário - ' + item.display_name,
                    start: item.profile.data.date_of_birth
                };
            })
            //console.log("Source",$scope.eventSources);
            //$scope.uiConfig.calendars['myCalendar'].fullCalendar('refetchEvents');
        })


    }
    newQuery(){
        return {
            limit: 1000,
            company_id : this.Auth.getCurrentCompanyId()
        }
    }
}
