export class WelcomeIndexController {
  constructor ($scope, Auth) {
    'ngInject';

    $scope.user = Auth.user();
    $scope.company = {trending_name : "DBR.ag"}
  }
}
