/**
 * Created by robin on 22/09/16.
 */
export class DocumentsIndexController{
    constructor ($scope, Document, Config, $stateParams , Auth, Folders, DocumentFiles) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;
        //define filters
        $scope.searchParams = {
            name: null,
            folder : null,
            user_id: null
        }
        $scope.folders = Folders.all();
        $scope.params = angular.copy($scope.searchParams);
        //watch for search event
        $scope.$on('searchChanged', function (event,data,params) {
            if(params)
                $scope.params = params;

            $scope.load();
        }, true );

        $scope.pagination = angular.extend( Config.pagination , { current_page: $stateParams.page ? $stateParams.page : 1 });
        $scope.list = [];

        $scope.load = function () {
            delete $scope.resourceItem;
            $scope.resourceItem = Document.query( angular.extend( $scope.pagination , _this.newQuery(), $scope.params ) , function (response) {
                $scope.list = response.data;
                $scope.pagination = response.meta.pagination;
            });
        }

        $scope.paginateTo = function (newPageNumber) {
            $scope.pagination.page = newPageNumber;
            $scope.load();
        }

        $scope.getToEdit = function (item) {
            $scope.currentItemToEdit = item;
        }

    }
    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }
}
