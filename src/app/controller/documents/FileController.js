/**
 * Created by robin on 22/09/16.
 */
export class FileController{
    constructor ($scope, DocumentFiles, Auth, $uibModal) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;
        $scope.document = DocumentFiles;

        $scope.getType = function(file){
            return $scope.document.getType( $scope.parseFile(file) );
        }
        $scope.getFileType = function(file){
            return $scope.document.getFileType($scope.parseFile(file));
        }
        $scope.getIconType = function(file){
            return $scope.document.getIconType($scope.parseFile(file));
        }

        $scope.remove = function () {
            //@todo remove files;
        }

        $scope.parseFile = function (file) {
            return angular.extend( file , {} );
        }
        $scope.modal = null;
        $scope.openOrPreview = function (file) {
            $scope.currentFile = file;
            if($scope.getFileType(file) == 'image'){
                //preview
                $scope.modal = $uibModal.open({
                    size: 'xs',
                    templateUrl: 'app/view/documents/modal.html',
                    scope: $scope
                });
            }else{
                window.open( file.file.original );
            }
        }

    }

}
