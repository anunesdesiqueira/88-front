/**
 * Created by robin on 22/09/16.
 */
export class DocumentsRegisterController{
    constructor ($scope, Auth, Folders, Team, $uibModal , DocumentFiles, Document) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;

        $scope.title = "Cadastro de documentos";

        $scope.types = [];

        $scope.loadDependencies = function () {
            $scope.folders = Folders.all();
            Team.query( _this.newQuery() , function (response) {
                $scope.users = response.data;
            } )
        }

        $scope.saveOrUpdate = function () {
            if(!$scope.document.$resolved){
                return false;//is resolving first click;
            }
            $scope.document.save( $scope.success )
        }

        $scope.success = function (response) {
            delete $scope.document;
            //$scope.document = $scope.newDocument({});
            $scope.params.time = moment().format();
            $scope.$emit("searchChanged");
        }

        $scope.newDocument = function (data) {
            return DocumentFiles;
        }

        $scope.$watch("currentItemToEdit", function (data) {
            if(data){
                delete $scope.document;
                //$scope.document = $scope.newDocument(data);
            }

        })
        $scope.loadDependencies();
        $scope.document = $scope.newDocument({});

    }

    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }


}
