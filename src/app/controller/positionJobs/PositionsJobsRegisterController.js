/**
 * Created by robin on 22/09/16.
 */
export class PositionsJobsRegisterController{
    constructor ($scope, PositionsJobs, Auth) {
        'ngInject';

        $scope.title = "Cadastro de cargos";

        $scope.saveOrUpdate = function () {
            if(!$scope.positionJob.$resolved){
                return false;//is resolving first click;
            }
            $scope.positionJob.$resolved = false;
            if($scope.positionJob.id){
                $scope.positionJob.$update($scope.success)
            }else{
                $scope.positionJob.$save($scope.success)
            }
        }

        $scope.success = function (response) {
            delete $scope.positionJob;
            $scope.positionJob = $scope.newPositionsJob({});
            $scope.params.time = moment().format();
            $scope.$emit("resourceDepartmentSuccess", response );
        }

        $scope.newPositionsJob = function (data) {
            return new PositionsJobs(angular.extend({
                company_id: Auth.getCurrentCompany().id,
                $resolved : true
            },data));
        }

        $scope.positionJob = $scope.newPositionsJob({});
    }

}
