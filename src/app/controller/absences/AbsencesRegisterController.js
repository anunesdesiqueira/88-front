/**
 * Created by robin on 04/09/16.
 */
export class AbsencesRegisterController{
    constructor ($scope, Absence , User, Auth, $uibModal, AbsencesStatus) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;

        $scope.title = "Cadastro faltas";

        $scope.loadDependencies = function () {
            $scope.statuses = AbsencesStatus.all();

            User.query({}, function (response) {
                $scope.users = response.data;
            });
        }

        $scope.saveOrUpdate = function () {
            if(!$scope.absence.$resolved){
                return false;//is resolving first click;
            }
            $scope.absence.$resolved = false;

            //format name
            if($scope.absence.user){
                $scope.absence.name = $scope.absence.user.display_name;
                $scope.absence.user_id = $scope.absence.user.id;
            }
            if($scope.absence.id){
                $scope.absence.$update($scope.success)
            }else{
                $scope.absence.$save($scope.success)
            }
        }

        $scope.success = function (response) {
            delete $scope.absence;
            $scope.absence = $scope.newAbsence({});
            $scope.params.time = moment().format();
            $scope.$emit("searchChanged");
        }

        $scope.newAbsence = function (data) {
            return new Absence(angular.extend({
                starting : null,
                ending : null,
                one_day: false,
                company_id: Auth.getCurrentCompany().id,
                $resolved : true
            },data));
        }

        $scope.$watch("currentItemToEdit", function (data) {
            if(data){
                $scope.absence = $scope.newAbsence(data);
                $scope.absence.date = moment($scope.absence.date).format("DD/MM/YYYY");

            }

        })
        $scope.loadDependencies();
        $scope.absence = $scope.newAbsence({});
        $scope.modal = null;

        $scope.setOneDay = function () {
            if($scope.absence.one_day){
                $scope.absence.ending = angular.copy($scope.absence.starting);
            }else{
                $scope.absence.ending = null;
            }
        }
    }
    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }


}
