/**
 * Created by robin on 07/09/16.
 */
export class TeamIndexController{
    constructor ($scope, Team, $stateParams, Config, Auth) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;
        //define filters
        $scope.searchParams = {
            email: null
        }
        $scope.params = angular.copy($scope.searchParams);
        //watch for search event
        $scope.$on('searchChanged', function (event,data,params) {
            if(params)
                $scope.params = params;

            $scope.load();
        }, true );

        $scope.pagination = angular.extend( Config.pagination , { current_page: $stateParams.page ? $stateParams.page : 1 });
        $scope.list = [];
        //
        //Team.query({}, function (response) {
        //    $scope.categories = response.data;
        //});

        $scope.load = function (params) {
            delete $scope.resourceItem;
            $scope.resourceItem = Team.query( angular.extend( $scope.pagination , _this.newQuery(), $scope.params ) , function (response) {
                $scope.list = response.data;
                $scope.pagination = response.meta.pagination;
            });
        }

        $scope.paginateTo = function (newPageNumber) {
            $scope.pagination.page = newPageNumber;
            $scope.load();
        }

    }
    newQuery(){
        return {
            company_id : this.Auth.getCurrentCompanyId()
        }
    }
}
