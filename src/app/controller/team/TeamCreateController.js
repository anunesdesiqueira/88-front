export class TeamCreateController {
    constructor($scope, Company, Departments, PositionsJobs, EmailSchema, Invitation, $stateParams , Auth, Config, Roles, $rootScope, $uibModal, $q) {
        'ngInject';
        var _this = this;
        _this.Auth = Auth;
        $scope.limit = Config.companies.usersLimit;
        $rootScope.hideHeader = true;
        $scope.isLoading = false;
        //define filters
        $scope.searchParams = {
            name: null,
            //benefit_category_id: null,
            //provider_id: null
        }
        $scope.params = angular.copy($scope.searchParams);
        //watch for search event
        $scope.$on('searchChanged', function (event, data, params) {
            if (params)
                $scope.params = params;

            $scope.load();
        }, true);

        $scope.pagination = angular.extend(Config.pagination, {current_page: $stateParams.page ? $stateParams.page : 1});
        $scope.list = [];

        $scope.roles = Roles.all();

        Company.query({} , function (response) {
            $scope.companies = response.data;
        })

        Departments.query( _this.newQuery() , function (response) {
            $scope.departments = response.data;
        });

        PositionsJobs.query( _this.newQuery() , function (response) {
            $scope.positionsJobs = response.data;
        });

        $scope.createInvitations = function () {
            for(var num = $scope.list.length; num < $scope.limit; num++){
                var invitation = new Invitation({
                    company_id: _this.Auth.getCurrentCompanyId(),
                    $resolved : true,
                    created : true
                });

                $scope.list.push(invitation);
            }
        }

        $scope.countNumbers = function () {
            return _.filter( $scope.list , { created: false }).length;
        }

        $scope.load = function () {
            delete $scope.resourceItem;
            $scope.resourceItem = Invitation.query(angular.extend($scope.pagination, _this.newQuery(), $scope.params), function (response) {
                $scope.list = response.data;
                $scope.pagination = response.meta.pagination;
                $scope.createInvitations();
            });
        }

        $scope.load();

        $scope.paginateTo = function (newPageNumber) {
            $scope.pagination.page = newPageNumber;
            $scope.load();
        }

        $scope.getToEdit = function (item) {
            $scope.currentItemToEdit = item;
        }

        $scope.modal = null;
        $scope.addDepartment = function(invitation) {
            $scope.currentInvitation = invitation;
            $scope.modal = $uibModal.open({
                size: 'xs',
                templateUrl: 'app/view/team/department.html',
                scope: $scope
            });
            $scope.modal.result.then(function (response) {
                $scope.departments.push(response);
                $scope.currentInvitation.department_id = response.id;
            })
        }

        $scope.$on("resourceDepartmentSuccess", function (event, data) {
            $scope.modal.close(data.data);
        })

        $scope.addPositionJob = function(invitation) {
            $scope.currentInvitation = invitation;
            $scope.modal = $uibModal.open({
                size: 'xs',
                templateUrl: 'app/view/team/positionJob.html',
                scope: $scope
            });
            $scope.modal.result.then(function (response) {
                $scope.positionsJobs.push(response);
                $scope.currentInvitation.position_job_id = response.id;
            })
        }

        $scope.$on("resourcePositionJobsSuccess", function (event, data) {
            $scope.modal.close(data.data);
        })

        $scope.saveOrUpdate = function () {
            $scope.isLoading = true;
            console.log($scope.isLoading);

            var promisses = [];
            _.each( $scope.list , function (invitation) {
                if(!invitation.name || !invitation.email){
                    return;
                }

                if( invitation.created ){
                    promisses.push(invitation.$save().$promise);
                }else{
                    promisses.push( Invitation.update( invitation ).$promise );
                }
            })
            $q.all(promisses).then( $scope.success, $scope.error );
        }

        $scope.success = function (response) {
            $scope.isLoading = false;
            console.log("$scope.success");
            console.log($scope.isLoading);
            console.log(response);
        }

        $scope.error = function (error) {
            $scope.isLoading = false;
            console.log("$scope.success")
            console.log($scope.isLoading);
            console.log(error);
        }

    }

    newQuery() {
        return {
            company_id: this.Auth.getCurrentCompanyId()
        }
    }
}
