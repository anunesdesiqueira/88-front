/**
 * Created by robin on 03/09/16.
 */
export class CompaniesChangeController{
    constructor ($scope, $localStorage, $state, UserService) {
        'ngInject';

        $scope.list = UserService.companies();


        $scope.change = function (item) {
            $localStorage.currentCompany = item;
            $state.go('app.index', { company_url: $localStorage.currentCompany.url }, {
                notify: true,
                reload: true
            });
        }
    }
}