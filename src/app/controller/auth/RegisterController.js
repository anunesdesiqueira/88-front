export class RegisterController {
  constructor ($scope, User, Auth, $state , $stateParams , Invitation ) {
    'ngInject';

    this.user = User;

    $scope.user= {
      //device : "mobile",
      remember_me: true,
      credentials: {
        email: '',
        password: '',
        terms_of_use : false,
        password_confirmation : '',
        authcode : $stateParams.authcode
      },
      code: ''
    };
    $scope.addUser = function(){
      User.save( angular.extend( $scope.user.credentials ) , function(response){
        if( $stateParams.authcode ){
          Invitation.accepted( {
            user_id : response.data.id,
            authcode: $stateParams.authcode
          } , function ( response ) {
            $scope.redirect();
          }, function (error) {

          } )
        }else{
          $scope.redirect();
        }
      })
    };

    $scope.redirect = function () {
      $state.go('auth.login', { }, {
        notify: true,
        reload: true
      });
    }
  }
}
