export class AuthIndexController {
  constructor ($uibModal, $scope) {
    'ngInject';

    $scope.termsOpen = function() {
      $uibModal.open({
        templateUrl: 'app/view/auth/termos.html',
      });
    }

  }
}
