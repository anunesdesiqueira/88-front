export class LoginController {
    constructor($scope, $state, Auth, $stateParams, Invitation, $auth, Config) {
        'ngInject';
        $scope.user = {
            remember_me: true,
            username: null,
            password: null
        };

        $scope.signIn = function () {

            Auth.authenticate($scope.user).then(function () {
                var user = Auth.user();
                if ($stateParams.authcode) {
                    Invitation.accepted({authcode: $stateParams.authcode}, {
                        user_id: user.id,
                        authcode: $stateParams.authcode
                    }, function (response) {
                        $scope.redirect(user);
                    }, function (error) {

                    })
                } else {
                    $scope.redirect(user);
                }
            });
        };

        $scope.redirect = function (user) {
            $state.go((!user.profile ? 'welcome.company' : 'start'), {}, {
                notify: true,
                reload: true
            });
        }

        $scope.logout = function () {
            Auth.logout().then(function () {
                $state.go('auth', {}, {
                    notify: true,
                    reload: true
                });
            });
        };

        $scope.setPassword = function () {
            $scope.user.credentials.email = $stateParams.email;
            $scope.user.code = $stateParams.code;
            Auth.activation($scope.user).then(function (response) {
                $state.go((response.confirmation ? 'confirmation' : 'app.index'), {}, {
                    notify: true,
                    reload: true
                });
            });
        };

        $scope.forgetPassword = function () {
            Auth.forget($scope.user.credentials).then(function () {
                $state.go('auth', {}, {
                    notify: true,
                    reload: true
                });
            });
        };

        $scope.resetPassword = function () {
            $scope.user.credentials.email = $stateParams.email;
            $scope.user.code = $stateParams.code;
            Auth.reset($scope.user).then(function () {
                $state.go('auth', {}, {
                    notify: true,
                    reload: true
                });
            });
        };

        $scope.authenticate = function(provider){
            $auth.authenticate(provider,Config.api.credentials).then(function(response) {
                // Signed in with Google.
                Auth.google(response).then(function (user) {
                    $scope.redirect(user);
                }, function (err) {

                });
            })
            .catch(function(response) {
                console.log("error", response)
                // Something went wrong.
            });
        }
    }
}
