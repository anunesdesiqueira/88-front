/**
 * Created by robin on 06/09/16.
 */
export class UserService {
    constructor( Auth  ) {
        'ngInject';

        this._user = Auth.user();
        this._company_id = Auth.getCurrentCompanyId();

    }

    userId(){
        return this.user().id;
    }
    user(){
        return this._user;
    }
    companies(){
        return this._user.companies;
    }
    getAvatarUrl(){
        if(!this._user.avatar)
            return null;

        return this._user.avatar.file ? this._user.avatar.file.thumb : null;
    }
    roles(){
        //apply filter
        var roles = [];
        if(this._user.roleUser){
            _.each(this._user.roleUser , function (item) {
                roles.push({
                    company_id: item.company_id,
                    roles : _.pick( item.role.data , 'description' , 'display_name' , 'name' )
                });
            });
        }
        return roles;
    }
    rolesInCompany( company_id ){
        var companyRole = _.findWhere( this.roles() , { company_id : company_id } );

        if(companyRole)
            return companyRole.roles;

        return false;
    }
    rolesInCurrentCompany(){
        //company_id
        return this.rolesInCompany( this._company_id );
    }
    isRole( test ){
        var roles = this.rolesInCurrentCompany();

        if(roles){
            if(roles.name == test){
                return true;
            }
        }

        return false;
    }
    isAdmin( roles ){
        return this.isRole( "admin" , roles );
    }
    isTeam( roles ){
        return this.isRole( "team" , roles );
    }
    isSuperAdmin( roles ){
        return this.isRole( "super" , roles );
    }

}
