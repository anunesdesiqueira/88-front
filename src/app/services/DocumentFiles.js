/**
 * Created by robin on 22/09/16.
 */
export class DocumentFiles {
    constructor( Auth , Upload, Document, $q, Config ) {
        'ngInject';
        var _this = this;
        this.Auth = Auth;
        this.Upload = Upload;
        this.Document = Document;
        this.$q = $q;
        this.Config = Config;
        this.$resolved = true;
        _this.url = _this.Config.base +'/companies/' + _this.Auth.getCurrentCompanyId() + '/documents';
        _this.queue = null;
        _this.file = null;
        _this.cropped = '';
        _this.fileModel = '';
        _this.files = [];
        _this.model = null;
        _this.resource = null;
        _this.progress = 0;
        _this.data = _this.newDocument();
    }

    setUrl(url){
        this.url = this.Config.base + url;
    }

    extend( obj ){
        this.data = angular.extend( this.data , obj);
    }

    setFile( file, errFiles){
        var _this = this;
        this.data.file = file;
        this.errFiles = errFiles;
        if(file){
            this.model = _this.getFileModel(file);
            this.files = [this.model];
        }else{
            this.files = [];
            this.model = null;
        }
    }

    remove(){
        this.setFile(null,null);
    }

    save(){
        var _this = this;
        _this.queue = _this.$q.defer();
        this.$resolved = false;
        this.resource = _this.Upload.upload({
            url: _this.url,
            fields: _this.data,
            file: _this.cropped ? _this.Upload.dataUrltoBlob(_this.cropped, _this.fileModel.name) : _this.file
        });
        this.resource.then(function (response) {
            _this.success(response);
        } , function (error) {
            _this.error(error);
        } , function (event) {
            _this.loading(event);
        } )

        return _this.queue.promise;
    }

    success(response){
        this.$resolved = true;
        this.queue.resolve(response);
    }
    error(response){
        this.$resolved = true;
        this.queue.reject(response);
    }
    loading(event){
        this.progress = parseInt(100.0 * event.loaded / event.total);
    }
    newDocument(){
        return {
            name: null,
            slug: 'document',
            reference: null,
            company_id: this.Auth.getCurrentCompanyId(),
            access: 'private',
            folder: null,
            user_id: null
        }
    }

    getFileModel(file){
        var _this = this;
        if(!file){
            return null;
        }
        return {
            name : file.name,
            size: file.size,
            type: _this.getType(file),
            filetype: _this.getFileType(file),
            iconType: _this.getIconType(file)
        }
    }
    getType(file){
        if(file.file_file_name){
            return file.file_file_name.split('.').pop();
        }
        return file.name.split('.').pop();
    }
    getIconType(file){
        return '#icon-' + this.getFileType(file);
    }
    getFileType(file){
        var type = this.getType(file);
        var images = ["png" , "jpg", "jpeg", "gif"];
        var listed = ["doc" , "docx", "xls", "zip", "pdf"];
        if(_.contains(images,type)){
            return "image";
        }else if(_.contains(listed,type)){
            return type;
        }else{
            return 'file';
        }
    }

}
