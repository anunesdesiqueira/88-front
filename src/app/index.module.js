/* global malarkey:false, moment:false */

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';

// CONTROLLERS
import { StartController } from './controller/app/StartController';
import { AuthIndexController } from './controller/auth/AuthIndexController';
import { LoginController } from './controller/auth/LoginController';
import { RegisterController } from './controller/auth/RegisterController';
import { ForwardController } from './controller/auth/ForwardController';
import { WelcomeIndexController } from './controller/welcome/WelcomeIndexController';

import { AppController } from './controller/app/AppController';
import { AppLogoutController } from './controller/app/AppLogoutController';
import { DashboardController } from './controller/app/DashboardController';
import { SearchController } from './controller/app/SearchController';

import { GuideIndexController } from './controller/guide/GuideIndexController';
import { RegisterIndexController } from './controller/register/RegisterIndexController';
import { RegisterCompanyController } from './controller/register/RegisterCompanyController';
import { RegisterPersonalController } from './controller/register/RegisterPersonalController';
import { RegisterBranchController } from './controller/register/RegisterBranchController';
import { RegisterAdditionalController } from './controller/register/RegisterAdditionalController';
import { RegisterEmployeController } from './controller/register/RegisterEmployeController';
import { PlansIndexController } from './controller/plans/PlansIndexController';
import { PaymentIndexController } from './controller/payment/PaymentIndexController';
import { EquipIndexController } from './controller/equip/EquipIndexController';
import { ControlIndexController } from './controller/control/ControlIndexController';
import { ControlRefundController } from './controller/control/ControlRefundController';

// INDIVIDUAL COMPONENTS CONTROLLERS
import { TableController } from './controller/components/TableController';
import { ToggleController } from './controller/components/ToggleController';
import { SelectController } from './controller/components/SelectController';
import { MenuController } from './controller/components/MenuController';
import { TabController } from './controller/components/TabController';

//  DIRECTIVES
import { DinamicSelect } from './directives/dinamic-select';
import { ConvertDate } from './directives/convert-date';

//Resources
import { MessageInterceptor } from './resources/MessageInterceptor';
import { Config } from './resources/Config';
import { TokenHandler } from './resources/TokenHandler.js';
import { Auth } from './resources/Auth';
import { User } from './resources/User';
import { UserProfile } from './resources/UserProfile';
import { Company } from './resources/Company';
import { Invitation } from './resources/Invitation';
import { TaxFrameworks } from './resources/TaxFrameworks';
import { Sectors } from './resources/Sectors';
import { Departments } from './resources/Departments';
import { PositionsJobs } from './resources/PositionsJobs';
//services
import { UserService } from './services/UserService';
import { DocumentFiles } from './services/DocumentFiles';

import { AddressSchema } from './schemas/AddressSchema';
import { EmailSchema } from './schemas/EmailSchema';
import { PhoneSchema } from './schemas/PhoneSchema';
import { RefundsStatus } from './schemas/RefundsStatus';
import { VacationsStatus } from './schemas/VacationsStatus';
import { LicensesStatus } from './schemas/LicensesStatus';
import { AbsencesStatus } from './schemas/AbsencesStatus';
import { Roles } from './schemas/Roles';
import { Folders } from './schemas/Folders';

//Features
//Control
import { ControlController } from './controller/control/ControlController';
//Refunds
import { RefundsIndexController } from './controller/refunds/RefundsIndexController';
import { RefundsRegisterController } from './controller/refunds/RefundsRegisterController';
import { Refund } from './resources/Refund';
import { TypeExpenditure } from './resources/TypeExpenditure';
//Vacations
import { VacationsIndexController } from './controller/vacations/VacationsIndexController';
import { VacationsRegisterController } from './controller/vacations/VacationsRegisterController';
import { VacationsCollectiveRegisterController } from './controller/vacations/VacationsCollectiveRegisterController';
import { Vacation } from './resources/Vacation';
//Licenses
import { LicensesIndexController } from './controller/licenses/LicensesIndexController';
import { LicensesRegisterController } from './controller/licenses/LicensesRegisterController';
import { License } from './resources/License';
//Absences
import { AbsencesIndexController } from './controller/absences/AbsencesIndexController';
import { AbsencesRegisterController } from './controller/absences/AbsencesRegisterController';
import { Absence } from './resources/Absence';
//Companies
import { CompaniesChangeController } from './controller/companies/CompaniesChangeController';
//Extra
import { ExtraController } from './controller/extra/ExtraController';
//Benefits
import { BenefitsIndexController } from './controller/benefits/BenefitsIndexController';
import { BenefitsRegisterController } from './controller/benefits/BenefitsRegisterController';
import { Benefit } from './resources/Benefit';
import { BenefitCategory } from './resources/BenefitCategory';
//Providers
import { ProvidersIndexController } from './controller/providers/ProvidersIndexController';
import { ProvidersRegisterController } from './controller/providers/ProvidersRegisterController';
import { Provider } from './resources/Provider';
import { ProviderCategory } from './resources/ProviderCategory';
//Team
import { TeamIndexController } from './controller/team/TeamIndexController';
import { TeamCreateController } from './controller/team/TeamCreateController';
import { DepartmentRegisterController } from './controller/departments/DepartmentRegisterController';
import { PositionsJobsRegisterController } from './controller/positionJobs/PositionsJobsRegisterController';
import { Team } from './resources/Team';
//Calendar
import { CalendarIndexController } from './controller/calendar/CalendarIndexController';
//Documents
import { DocumentsIndexController } from './controller/documents/DocumentsIndexController';
import { DocumentsRegisterController } from './controller/documents/DocumentsRegisterController';
import { FileController } from './controller/documents/FileController';
import { Document } from './resources/Document';

angular.module('88', [
  'ngAnimate',
  'ngCookies',
  'ngTouch',
  'ngSanitize',
  'ngMessages',
  'ngAria',
  'ngResource',
  'ui.router',
  'ui.bootstrap',
  'toastr',
  'ngFileUpload',
  'ngImgCrop',
  'ncy-angular-breadcrumb',
  'selector',
  'ngStorage',
  'angular-oauth2',
  'angular-growl',
  'google.places',
  'ui.utils.masks',
  'angularUtils.directives.dirPagination',
  'angularMoment',
  'ui.calendar',
  '720kb.tooltips',
  'satellizer'
])
  .constant('malarkey', malarkey)
  .constant('moment', moment)
  .service('MessageInterceptor', MessageInterceptor)
  .constant('Config', new Config)
  .config(config)
  .config(routerConfig)
  .run(runBlock)

  .controller('AuthIndexController', AuthIndexController)
  .controller('LoginController', LoginController)
  .controller('RegisterController', RegisterController)
  .controller('ForwardController', ForwardController)
  .controller('WelcomeIndexController', WelcomeIndexController)
  //app
  .controller('StartController', StartController)
  .controller('AppController', AppController)
  .controller('AppLogoutController', AppLogoutController)
  .controller('DashboardController', DashboardController)
  .controller('SearchController', SearchController)
  //feature - companies
  .controller('CompaniesChangeController', CompaniesChangeController)
  //Control
  .controller('ControlController', ControlController)
  //feature - Refunds
  .controller('RefundsIndexController', RefundsIndexController)
  .controller('RefundsRegisterController', RefundsRegisterController)
  .service('Refund', Refund)
  .service('TypeExpenditure', TypeExpenditure)
  //feature - Vacations
  .controller('VacationsIndexController', VacationsIndexController)
  .controller('VacationsRegisterController', VacationsRegisterController)
  .controller('VacationsCollectiveRegisterController', VacationsCollectiveRegisterController)
  .service('Vacation', Vacation)
  //feature - Licenses
  .controller('LicensesIndexController', LicensesIndexController)
  .controller('LicensesRegisterController', LicensesRegisterController)
  .service('License', License)
  //feature - Absences
  .controller('AbsencesIndexController', AbsencesIndexController)
  .controller('AbsencesRegisterController', AbsencesRegisterController)
  .service('Absence', Absence)
  .service('TypeExpenditure', TypeExpenditure)
  //Extra
  .controller('ExtraController', ExtraController)
  //feature - benefits
  .controller('BenefitsIndexController', BenefitsIndexController)
  .controller('BenefitsRegisterController', BenefitsRegisterController)
  .service('Benefit', Benefit)
  .service('BenefitCategory', BenefitCategory)
  //feature - providers
  .controller('ProvidersIndexController', ProvidersIndexController)
  .controller('ProvidersRegisterController', ProvidersRegisterController)
  .service('Provider', Provider)
  .service('ProviderCategory', ProviderCategory)
  //feature - documents
  .controller('DocumentsIndexController', DocumentsIndexController)
  .controller('DocumentsRegisterController', DocumentsRegisterController)
  .controller('FileController', FileController)
  .service('Document', Document)

  //Team
  .controller('TeamIndexController', TeamIndexController)
  .controller('TeamCreateController', TeamCreateController)
  .controller('DepartmentRegisterController', DepartmentRegisterController)
  .controller('PositionsJobsRegisterController', PositionsJobsRegisterController)
  .service('Team', Team)
  //Calendar
  .controller('CalendarIndexController', CalendarIndexController)

  .controller('GuideIndexController', GuideIndexController)
  .controller('RegisterIndexController', RegisterIndexController)
  .controller('RegisterCompanyController', RegisterCompanyController)
  .controller('RegisterPersonalController', RegisterPersonalController)
  .controller('RegisterBranchController', RegisterBranchController)
  .controller('RegisterAdditionalController', RegisterAdditionalController)
  .controller('RegisterEmployeController', RegisterEmployeController)
  .controller('PlansIndexController', PlansIndexController)
  .controller('PaymentIndexController', PaymentIndexController)
  .controller('EquipIndexController', EquipIndexController)
  .controller('ControlIndexController', ControlIndexController)
  .controller('ControlRefundController', ControlRefundController)

  .controller('TableController', TableController)
  .controller('ToggleController', ToggleController)
  .controller('SelectController', SelectController)
  .controller('MenuController', MenuController)
  .controller('TabController', TabController)

  .directive('dinamicSelect', DinamicSelect)
  .directive('convertDate', ConvertDate )
  .service('User', User)
  .service('UserProfile', UserProfile)
  .service('Company', Company)
  .service('Invitation', Invitation)
  .service('TaxFrameworks', TaxFrameworks)
  .service('Sectors', Sectors)
  .service('Departments', Departments)
  .service('PositionsJobs', PositionsJobs)

  .service('TokenHandler', TokenHandler)
  .service('Auth', Auth )
  .service('AddressSchema', AddressSchema )
  .service('EmailSchema', EmailSchema )
  .service('PhoneSchema', PhoneSchema )
  .service('RefundsStatus', RefundsStatus )
  .service('VacationsStatus', VacationsStatus )
  .service('LicensesStatus', LicensesStatus )
  .service('AbsencesStatus', AbsencesStatus )
  .service('Roles', Roles )
  .service('Folders', Folders )
  .service('UserService', UserService )
  .service('DocumentFiles', DocumentFiles )
