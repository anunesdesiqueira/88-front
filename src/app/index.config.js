export function config ($logProvider, toastrConfig, $breadcrumbProvider, OAuthProvider, OAuthTokenProvider, Config, growlProvider, $httpProvider, tooltipsConfProvider, $authProvider) {
  'ngInject';
  // Enable log
  $logProvider.debugEnabled(true);
  $breadcrumbProvider.setOptions({
    templateUrl: 'app/view/partials/template-breadcrubs.html'
  });

  tooltipsConfProvider.configure({
    'smart':true,
    'size':'large',
    'speed': 'slow',
    'tooltipTemplateUrlCache': true
  });

  // Set options third-party lib
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.preventDuplicates = true;
  toastrConfig.progressBar = true;

  OAuthTokenProvider.configure({
    name: 'token',
    options: {
      secure: false
    }
  });

  $authProvider.google({
    url: Config.base + '/oauth/access_token',
    clientId: Config.google.client_id,
    withCredentials : true

  });

  OAuthProvider.configure({
    name: 'data',
    baseUrl: Config.base,
    grantPath: 'oauth/access_token',
    revokePath: 'oauth/logout',
    clientId: Config.api.credentials.client_id,
    clientSecret: Config.api.credentials.client_secret
  });

  growlProvider.globalTimeToLive(5000);
  growlProvider.globalDisableCountDown(true);
  growlProvider.globalDisableIcons(true);
  growlProvider.globalPosition('top-right');

  $httpProvider.interceptors.push('MessageInterceptor');
}
