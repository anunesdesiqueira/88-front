export class Config{
   constructor(){
       'ngInject';
       var environment,baseUrl;
       var local = 'http://api.oitentaeoito.com/v1';
       var _this = this;
       var hosts = [
           {type : 'staging', api: 'http://devapi.oitentaeoito.com.vc/v1', list : [ "88.app.dbr.ag" , "www.88.app.dbr.ag" , "dev.oitentaeoito.com.vc" , "www.dev.oitentaeoito.com.vc" ]},
           {type : 'production', api: 'http://api.oitentaeoito.com.vc/v1', list : [ "app.oitentaeoito.com.vc" , "www.app.oitentaeoito.com.vc" ]}
       ];

       _.each(hosts , function (host) {
           if( _.contains(host.list, window.location.host) ){
                environment = host.type;
           }

           if(environment){
               _this.base = host.api;
           }else{
               _this.base = local;
           }

       });

       this.hostApi = null;//'http://apioitentaeoito.app.dbr.ag';
       this.isOnlineUrl = null;//'http://apioitentaeoito.app.dbr.ag';
       this.webVersion = "0.0.2";
       this.version = "0.0.2";

       this.pagination = {
           limit : 15
       }

       this.date = {
           short : 'DD/MM/YYYY',
           mysql : 'YYYY-MM-DD'
       }

       this.companies = {
           usersLimit : 5
       }

       this.google = {
           client_id : '957595828821-7bkimvs1amk0i3aeo82357c9nhs90lrp.apps.googleusercontent.com'
       }

       this.api = {
           credentials: {
               client_id: 'd41d8cd98f00b204e9800998ecf8427e',
               client_secret: 'horadoshow',
               grant_type : 'password'
           }
       }
   }
}
