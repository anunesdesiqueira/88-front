export class Document {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/companies/:company_id/documents/:id', { id: '@id' , company_id: '@company_id' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}