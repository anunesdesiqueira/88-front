/**
 * Created by robin on 01/09/16.
 */
export class OAuthProviderD{
    constructor(OAuthProvider,Config){
        'ngInject';
        OAuthProvider.configure({
            baseUrl: Config.base,
            clientId: 'd41d8cd98f00b204e9800998ecf8427e',
            clientSecret: 'horadoshow'
        });
        return OAuthProvider;
    }
}