/**
 * Created by robin on 01/09/16.
 */
export class MessageInterceptor{
    constructor($q, growl, $rootScope) {
        'ngInject';
        function checkResponse(response) {
            var message = "<ul>";

            if(!response.data)
                return false;

            if (response.data.errors) {

                angular.forEach(response.data.errors,function(values){
                    angular.forEach(values,function(error){
                        message += "<li>" + error + "</li>";
                    });
                });
                message += "</ul>";
                growl.error(message,{enableHtml: true});
            }

            if (response.data.message) {
                message = response.data.message;
                growl.success(message);
            }
        }

        return {
            // optional method
            'response': function (response) {
                // do something on success
                checkResponse(response);
                return response;
            },

            // optional method
            'responseError': function (rejection) {
                if (rejection.status === 401) {
                    $rootScope.$broadcast('unauthorized');
                }
                // do something on error
                checkResponse(rejection);
                return $q.reject(rejection);
            }
        }
    }
}