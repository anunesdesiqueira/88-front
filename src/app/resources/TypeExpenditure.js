export class TypeExpenditure{
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/expenditures/:id', { id: '@id' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}