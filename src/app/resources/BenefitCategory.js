export class BenefitCategory {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/benefits/categories/:id', { id: '@id' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}