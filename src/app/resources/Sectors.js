export class Sectors {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/sectors/:id', { id: '@id' }, {
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}