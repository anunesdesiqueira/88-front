export class Departments {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/companies/:company_id/departments/:id', { id: '@id' , company_id: '@company_id' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}