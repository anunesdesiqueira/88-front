export class TaxFrameworks {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/taxesFrameworks/:id', { id: '@id' }, {
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}