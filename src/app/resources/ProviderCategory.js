export class ProviderCategory {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/providers/categories/:id', { id: '@id' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}