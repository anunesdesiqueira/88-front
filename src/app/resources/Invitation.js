export class Invitation {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/companies/:company_id/invitations/:id', { id: '@id' , company_id: '@company_id' , authcode: '@authcode' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false },
            'accepted' : { method : 'POST', isArray: false , url: Config.base + '/invitations/accepted/:authcode' }
        });
        return resource;
    }
}