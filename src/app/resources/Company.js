export class Company {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/companies/:id', { id: '@id' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}