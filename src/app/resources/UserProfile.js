export class UserProfile {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/users/profiles/:id', { id: '@id' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}