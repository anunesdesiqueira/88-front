export class User {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/users/:id', { id: '@id' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false },
            'auth' : { method : 'GET', isArray: false, url: Config.base + '/users/auth' },
            'profile' : { method : 'GET', isArray: false , url: Config.base + '/users/:id/profile' }
        });
        return resource;
    }
}