export class Team {
    constructor ($resource, Config){
        'ngInject';
        var resource = $resource(Config.base + '/users/:id', { id: '@id' , company_id: '@company_id' }, {
            'query' : { method : 'GET', isArray:false }
        });
        return resource;
    }
}