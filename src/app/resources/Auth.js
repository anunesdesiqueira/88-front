export class Auth {
    constructor ($q, $http, $localStorage, Config, OAuth, OAuthToken, growl, $cookies, User, Company){
        'ngInject';
        this.$q = $q;
        this.$http = $http;
        this.$localStorage = $localStorage;
        this.Config = Config;
        this.OAuth = OAuth;
        this.OAuthToken = OAuthToken;
        this.growl = growl;
        this.$cookies = $cookies;
        this.User = User;
        this.Company = Company;
    }

    authenticate(credentials) {
        var $this = this;
        var q = this.$q.defer();
        this.OAuth.getAccessToken( credentials ).then(function () {
            $this.authUser().then(function () {
                q.resolve();
            }, function (err) {
                q.reject(err);
            })
        }, function (response) {
            if(response.data.error){
                var message = "<ul>";
                message += "<li>" + response.data.error_description + "</li>";
                message += "</ul>";
                $this.growl.error(message,{enableHtml: true});
            }
            q.reject(response);
        });
        return q.promise;
    }

    google(token_data){
        var $this = this;
        var q = this.$q.defer();
        this.OAuthToken.setToken( token_data.data );

        $this.authUser().then(function (user) {
            q.resolve(user);
        }, function (err) {
            q.reject(err);
        })

        return q.promise;
    }

    authUser(){
        var $this = this;
        var q = this.$q.defer();
        $this.User.auth( { include: 'roleUser' }, function (userResponse) {

            $this.$localStorage.user = userResponse.data;//save response from server

            if(userResponse.data.profile)
                $this.$localStorage.user.profile = userResponse.data.profile.data;//save response from server

            if(userResponse.data.roleUser)
                $this.$localStorage.user.roleUser = userResponse.data.roleUser.data;//save response from server

            $this.loadUserCompanies().then(function () {
                q.resolve($this.$localStorage.user);
            }, function (err) {
                q.reject(err);
            });
        },function(err){
            q.reject(err);
        });

        return q.promise;
    }

    loadUserCompanies(){
        var $this = this;
        var q = this.$q.defer();
        var companies;
        $this.Company.query( { filter: 'id;trading_name;type;slug' } , function (companyResponse) {
            companies = _.map(companyResponse.data , function (item) {
                return _.pick(item , 'id', 'trading_name' , 'slug', 'type');
            });//save companies list
            $this.setCompaniesList( companies );
            $this.setCurrentCompany( $this.checkWichCompany() );
            q.resolve(companies);
        }, function (err) {
            q.reject(err);
        });
        return q.promise;
    }

    user(){
        return this.$localStorage.user ? this.$localStorage.user : false;
    }

    profile(){
        return this.$localStorage.user.profile ? this.$localStorage.user.profile : false;
    }

    setProfile(data){
        return this.$localStorage.user.profile = data;
    }

    setAvatar(data){
        return this.$localStorage.user.avatar = data;
    }


    getCurrentCompany(){
        return this.$localStorage.currentCompany ? this.$localStorage.currentCompany : false;
    }

    getCurrentCompanyId(){
        return this.$localStorage.currentCompany ? this.$localStorage.currentCompany.id : false;
    }

    setCurrentCompany(company){
        this.$localStorage.currentCompany = company;
    }

    getCompaniesList(){
        return this.$localStorage.user.companies
    }
    setCompaniesList(companies){
        this.$localStorage.user.companies = companies;
    }

    checkWichCompany(){
        return _.first( this.getCompaniesList() );
    }

    isAuthenticated(){
        return this.OAuth.isAuthenticated();
    }
    refreshAccess(){
        return this.OAuth.getRefreshToken();
    }

    logout(){
        return this.OAuth.revokeToken();
    }

    clear(){
        this.$localStorage.$reset();
    }

    googleURL(){}

    //isAuthorized(authorizedRoles) {
    //    if (!angular.isArray(authorizedRoles)) {
    //        authorizedRoles = [authorizedRoles];
    //    }
    //    return (this.isAuthenticated() && authorizedRoles.indexOf(this.$localStorage.UserRoles) !== -1);
    //}

    //activation(data) {
    //    return this.$http.post(this.Config.base + '/auth/activation/' + data.credentials.email + '/' + data.code , data ).then(function(response) {
    //        this.$localStorage.userId = response.data.id;
    //        this.$localStorage.userToken = response.data.api_token;
    //        this. $localStorage.userData = {
    //            id: response.data.id,
    //            role: response.data.role,
    //            name: response.data.name,
    //            email: response.data.email,
    //            should_update_schedule: response.data.should_update_schedule,
    //            schedule_updated_at : response.data.schedule_updated_at,
    //            groups: response.data.groups,
    //            avatar: (response.data.profile ? response.data.profile.avatar : null)
    //        }
    //        return response;
    //    });
    //}
    //
    //forget(data) {
    //    return this.$http.post(this.Config.base + '/auth/forget/', data ).then(function(response) {
    //        return response;
    //    });
    //}

    //reset(data) {
    //    return this.$http.post(this.Config.base + '/auth/reset-password/' + data.credentials.email + '/' + data.code , data.credentials ).then(function(response) {
    //        this.$localStorage.userId = response.data.id;
    //        this.$localStorage.userToken = response.data.api_token;
    //        this. $localStorage.userData = {
    //            id: response.data.id,
    //            role: response.data.role,
    //            name: response.data.name,
    //            email: response.data.email,
    //            should_update_schedule: response.data.should_update_schedule,
    //            schedule_updated_at : response.data.schedule_updated_at,
    //            groups: response.data.groups,
    //            avatar: (response.data.profile ? response.data.profile.avatar : null)
    //        }
    //        return response;
    //    });
    //}
}