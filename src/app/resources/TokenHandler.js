export class TokenHandler{
    constructor(OAuthToken, $http) {
        'ngInject';
        var handler = this;
        handler.$http = $http;
        handler.OAuthToken = OAuthToken;

        //handler.setToken = function( newToken ) {
        //    handler.$localStorage.userToken = newToken;
        //};

        handler.getToken = function() {
            return handler.OAuthToken.getAccessToken();
        };

        handler.wrapActions = function( resource, actions ) {
            var wrappedResource = resource;
            for (var i=0; i < actions.length; i++) {
                handler.tokenWrapper( wrappedResource, actions[i] );
            }
            return wrappedResource;
        };

        handler.tokenWrapper = function( resource, action ) {
            resource['_' + action]  = resource[action];
            resource[action] = function(param, data, success, error) {

                var interceptSuccess = function(responseData, headers) {
                    if(headers){
                        var token = headers('X-Auth-Token');
                        if (angular.isFunction(success)) success(responseData, headers);
                    }
                };

                handler.$http.defaults.headers.common['X-Auth-Token'] = handler.getToken();
                return resource['_' + action](
                    angular.extend({}, param || {}),
                    data,
                    interceptSuccess,
                    error
                );
            };
        };
        return handler;

    }
}
