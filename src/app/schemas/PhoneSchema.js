function Phone(){
  var _this = this;
  _this.list = [];
  _this.format = { main: false, phone: '' }
  _this.newItem = function( obj ){
    var item = angular.copy( angular.extend(_this.format,obj) );
    _this.list.push(item);
    return item;
  }
  return this;

}
export class PhoneSchema {
  constructor( ) {
    'ngInject';
  }
  item(){
    var phone = new Phone();
    return phone.format;
  }
  start (obj, number) {
    var Item = new Phone();
    if(!number)
      number = 1;
    var i = 0;
    Item.newItem(obj);
    i++;
    while(i < number){
      Item.newItem();
      i++;
    }

    return Item.list;
  }
}
