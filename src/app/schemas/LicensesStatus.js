export class LicensesStatus {
  constructor( ) {
    'ngInject';
    this.list = [
      {name: 'Solicitado', slug: 'requested'},
      {name: 'Aprovado', slug: 'approved'},
      {name: 'Recusado', slug: 'refused'}
    ];
  }
  all () {
    return this.list;
  }
}
