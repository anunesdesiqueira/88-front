export class Folders {
  constructor( ) {
    'ngInject';
    this.list = [
      {name: 'Empresa', slug: 'company' , id : 1},
      {name: 'Equipe', slug: 'team' , id : 2},
      {name: 'Pessoal', slug: 'personal' , id : 3 }
    ];
  }
  all () {
    return this.list;
  }
}
