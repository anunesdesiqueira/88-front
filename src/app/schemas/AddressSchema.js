export class AddressSchema {
  constructor( ) {
    'ngInject';

    var _this = this;
    this.formatted = "";
    this.address = "";
    this.data_address = {
      street: "",
      number: "",
      neighborhood: "",
      complement: "",
      state: "",
      city: "",
      postal_code: "",
      country: "",
      lat: null,
      lng: null,
      search: ""
    }
    this.lat = null;
    this.lng = null;

    this.address_components = "";
    //format
    this.getComponents = function(){
      return [
        { selector: 'street_number', type: 'short_name', alias: 'number' },
        { selector: 'route', type: 'long_name', alias: 'street' },
        { selector: 'sublocality_level_1', type: 'short_name', alias: 'neighborhood' },
        { selector: 'administrative_area_level_1', type: 'short_name', alias: 'state' },
        { selector: 'locality', type: 'long_name', alias: 'city' },
        { selector: 'country', type: 'long_name', alias: 'country' },
        { selector: 'postal_code', type: 'short_name', alias: 'postal_code' },
        { selector: 'lat', type: 'short_name', alias: 'lat' },
        { selector: 'lng', type: 'short_name', alias: 'lng' }
      ]
    }

    this.set = function(data_address,dest){
      if(!data_address || typeof data_address != "object")
        return false;

      _this.address = data_address.formatted_address;
      _this.data_address.lat = _this.lat = data_address.geometry.location.lat ();
      _this.data_address.lng = _this.lng = data_address.geometry.location.lng ();

      var components = _this.getComponents();
      console.log(data_address.address_components);
      _.each ( data_address.address_components , function ( element, index ) {
        var item = _.findWhere ( components , { selector: element.types[ 0 ] } )
        if ( item ) {
          _this.data_address[ item.alias ] = element[ item.type ];
        }
      } );

      //angular.extend(dest,_this.getData());
      return true;
    }

    this.getData = function(){
      return {
        address: _this.address,
        data_address: _this.data_address,
        lat: _this.lat,
        lng: _this.lng
      }
    }
  }
}
