export class RefundsStatus {
  constructor( ) {
    'ngInject';
    this.list = [
      {name: 'Solicitado', slug: 'requested'},
      {name: 'Aprovado', slug: 'approved'},
      {name: 'Recusado', slug: 'refused'},
      {name: 'Pago', slug: 'paid'}
    ];
  }
  all () {
    return this.list;
  }
}
