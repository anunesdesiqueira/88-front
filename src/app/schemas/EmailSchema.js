function Email(){
  var _this = this;
  _this.list = [];
  _this.format = { main: false, email: '' }
  _this.newItem = function( obj ){
    var item = angular.copy( angular.extend(_this.format,obj) );
    _this.list.push(item);
    return item;
  }
  return this;

}
export class EmailSchema {
  constructor( ) {
    'ngInject';
  }
  start (obj, number) {
    var Item = new Email();
    if(!number)
      number = 1;
    var i = 0;
    Item.newItem(obj);
    i++;
    while(i < number){
      Item.newItem();
      i++;
    }

    return Item.list;
  }
}
