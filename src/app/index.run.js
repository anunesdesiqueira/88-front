export function runBlock ($log,$rootScope,$state,Auth,$window,amMoment) {
  'ngInject';
  $log.debug('runBlock end');

  var stateChenge = $rootScope.$on('$stateChangeStart', function (event, toState) {
    $rootScope.hideHeader = false;
    var openPages = ["auth.login","auth.password","home", "password",'mobile','auth.forget', 'auth.register' , 'calendar']
    var requireLogin = false;

    if (openPages.indexOf(toState.name) == -1) {
      requireLogin = true;
    }

    if (requireLogin && !Auth.isAuthenticated()) {
      event.preventDefault();
      $state.go("auth.login", {}, {
        notify: true,
        reload: true
      });
    }

  });

  var unauthorized = $rootScope.$on('unauthorized', function() {
    //Auth.forceLogout();
    $state.go("auth.login", {}, {
      notify: true,
      reload: true
    });
  });

  $rootScope.$on('oauth:error', function(event, rejection) {
    // Ignore `invalid_grant` error - should be catched on `LoginController`.
    if ('invalid_grant' === rejection.data.error) {
      return;
    }
    // Refresh token when a `invalid_token` error occurs.
    if ('invalid_token' === rejection.data.error) {
      return Auth.refreshAccess();
    }
    // Redirect to `/login` with the `error_reason`.
    return $window.location.href = '#/auth/login?error_reason=' + rejection.data.error;
  });

  amMoment.changeLocale('America/Sao_Paulo');
}
