export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider

      .state('start', {
        url: '/',
        //templateUrl: 'app/view/app/index.html',
        controller: 'StartController',
        controllerAs: 'starter',
        resolve : {
          currentCompany : function (Auth) {
            return Auth.getCurrentCompany();
          }
        }
      })
      .state('logout', {
        url: '/logout',
        controller: 'AppLogoutController',
        controllerAs: 'logout'
      })

      .state('app', {
        abstract: true,
        url: '/:company_url/',
        templateUrl: 'app/view/app/index.html',
        controller: 'AppController',
        controllerAs: 'app',
        resolve : {
          currentCompany : function (Auth) {
            return Auth.getCurrentCompany();
          }
        }

      })

      .state('app.index', {
        url: 'dashboard',
        templateUrl: 'app/view/dashboard/index.html',
        controller: 'DashboardController',
        controllerAs: 'dashboard',
        parent: 'app'
      })
      .state('app.team', {
        abstract: true,
        url: 'team',
        templateUrl: 'app/view/team/index.html',
        //controller: 'TeamIndexController',
        //controllerAs: 'team',
        parent: 'app'
      })
      .state('app.team.index', {
        url: '/',
        templateUrl: 'app/view/team/list.html',
        controller: 'TeamIndexController',
        controllerAs: 'team',
        parent: 'app.team',
        ncyBreadcrumb: {
          label: 'Equipe'
        }
      })
      .state('app.team.create', {
        url: '/create',
        templateUrl: 'app/view/team/create.html',
        controller: 'TeamCreateController',
        controllerAs: 'teamCreate',
        parent: 'app.team',
        ncyBreadcrumb: {
          label: 'Cadastro de usuários'
        }
      })

      .state('app.calendar', {
        url: '/calendar',
        templateUrl: 'app/view/calendar/index.html',
        controller: 'CalendarIndexController',
        controllerAs: 'calendar',
        parent: 'app',
        ncyBreadcrumb: {
          label: 'Calendário'
        }
      })

      .state('app.extra', {
        abstract: true,
        url: 'extra/',
        templateUrl: 'app/view/extra/index.html',
        controller: 'ExtraController',
        controllerAs: 'extra'
      })

      .state('app.extra.providers', {
        abstract: true,
        url: 'providers',
        templateUrl: 'app/view/providers/index.html',
        //controller: 'ProvidersIndexController',
        controllerAs: 'providers',
        parent: 'app.extra'
      })

      .state('app.extra.providers.index', {
        url: '/index',
        templateUrl: 'app/view/providers/list.html',
        controller: 'ProvidersIndexController',
        controllerAs: 'providersIndex',
        parent: 'app.extra.providers',
        ncyBreadcrumb: {
          label: 'Fornecedores'
        }
      })

      .state('app.extra.benefits', {
        abstract: true,
        url: 'benefits',
        templateUrl: 'app/view/benefits/index.html',
        controllerAs: 'benefits',
        parent: 'app.extra'
      })

      .state('app.extra.benefits.index', {
        url: '/index',
        templateUrl: 'app/view/benefits/list.html',
        controller: 'BenefitsIndexController',
        controllerAs: 'benefits',
        parent: 'app.extra.benefits',
        ncyBreadcrumb: {
          label: 'Benefícios'
        }
      })

      .state('app.extra.documents', {
        abstract: true,
        url: 'documents',
        templateUrl: 'app/view/documents/index.html',
        controllerAs: 'documents',
        parent: 'app.extra'
      })

      .state('app.extra.documents.index', {
        url: '/index',
        templateUrl: 'app/view/documents/list.html',
        controller: 'DocumentsIndexController',
        controllerAs: 'documents',
        parent: 'app.extra.documents',
        ncyBreadcrumb: {
          label: 'Documentos'
        }
      })

      .state('app.control', {
        abstract: true,
        url: 'control/',
        templateUrl: 'app/view/control/index.html',
        controller: 'ControlController',
        controllerAs: 'control'
      })

      .state('app.control.refunds', {
        abstract: true,
        url: 'refunds',
        templateUrl: 'app/view/refunds/index.html',
        controllerAs: 'refunds',
        parent: 'app.control'
      })

      .state('app.control.refunds.index', {
        url: '/index',
        templateUrl: 'app/view/refunds/list.html',
        controller: 'RefundsIndexController',
        controllerAs: 'refundsIndex',
        parent: 'app.control.refunds',
        ncyBreadcrumb: {
          label: 'Reembolsos'
        }
      })

      .state('app.control.vacations', {
        abstract: true,
        url: 'vacations',
        templateUrl: 'app/view/vacations/index.html',
        controllerAs: 'vacations',
        parent: 'app.control'
      })

      .state('app.control.vacations.index', {
        url: '/index',
        templateUrl: 'app/view/vacations/list.html',
        controller: 'VacationsIndexController',
        controllerAs: 'vacationsIndex',
        parent: 'app.control.vacations',
        ncyBreadcrumb: {
          label: 'Férias'
        }
      })

      .state('app.control.licenses', {
        abstract: true,
        url: 'licenses',
        templateUrl: 'app/view/licenses/index.html',
        controllerAs: 'licenses',
        parent: 'app.control'
      })

      .state('app.control.licenses.index', {
        url: '/index',
        templateUrl: 'app/view/licenses/list.html',
        controller: 'LicensesIndexController',
        controllerAs: 'licensesIndex',
        parent: 'app.control.licenses',
        ncyBreadcrumb: {
          label: 'Licenças'
        }
      })

      .state('app.control.absences', {
        abstract: true,
        url: 'absences',
        templateUrl: 'app/view/absences/index.html',
        controllerAs: 'absences',
        parent: 'app.control'
      })

      .state('app.control.absences.index', {
        url: '/index',
        templateUrl: 'app/view/absences/list.html',
        controller: 'AbsencesIndexController',
        controllerAs: 'absencesIndex',
        parent: 'app.control.absences',
        ncyBreadcrumb: {
          label: 'Faltas'
        }
      })


    .state('home', {
      url: '/home',
      templateUrl: 'app/view/guide/index.html',
      controller: 'GuideIndexController',
      controllerAs: 'home'
    })

    // GUIDELINE - APENAS COMPONENTS - START

    .state('guide-register', {
      url: '/guide-register',
      templateUrl: 'app/view/guide/register.html',
      controller: 'GuideIndexController',
      controllerAs: 'guide'
    })

    // GUIDELINE - APENAS COMPONENTS - END


    .state('auth', {
      abstract: true,
      url: '/auth',
      templateUrl: 'app/view/auth/index.html',
      controller: 'AuthIndexController',
      controllerAs: 'auth'
    })

      .state('auth.login', {
        url: '/login/:authcode',
        templateUrl: 'app/view/auth/login.html',
        controller: 'LoginController',
        parent: 'auth',
        controllerAs: 'login',
        ncyBreadcrumb: {
          label: 'Login'
        }
      })

        .state('auth.alternative-login', {
          url: '/alternative-login',
          templateUrl: 'app/view/auth/alternative-login.html',
          controller: 'LoginController',
          parent: 'auth',
          controllerAs: 'login',
          ncyBreadcrumb: {
            label: 'Login'
          }
        })

        .state('auth.register', {
          url: '/register/:authcode',
          templateUrl: 'app/view/auth/register.html',
          controller: 'RegisterController',
          controllerAs: 'register',
          ncyBreadcrumb: {
            label: 'Cadastro'
          }
        })

        .state('auth.forward', {
          url: '/forward',
          templateUrl: 'app/view/auth/forward.html',
          controller: 'ForwardController',
          controllerAs: 'forward',
          ncyBreadcrumb: {
            label: 'Esqueceu sua senha?'
          }
        })


    .state('welcome', {
      abstract: true,
      url: '/welcome',
      templateUrl: 'app/view/welcome/index.html',
      controller: 'WelcomeIndexController',
      controllerAs: 'welcome',
      ncyBreadcrumb: {
        label: 'Bem vindo'
      }
    })

      .state('welcome.company', {
        url: '/company',
        templateUrl: 'app/view/welcome/company.html',
        ncyBreadcrumb: {
          label: 'Bem vindo - company'
        }
      })

      .state('welcome.guest', {
        url: '/guest',
        templateUrl: 'app/view/welcome/guest.html',
        ncyBreadcrumb: {
          label: 'Bem vindo - guest'
        }
      })

    .state('faq', {
      url: '/faq',
      templateUrl: 'app/view/faq/index.html',
      ncyBreadcrumb: {
        label: 'FAQ'
      }
    })

    .state('register', {
      abstract: true,
      url: '/register',
      templateUrl: 'app/view/register/index.html',
      controller: 'RegisterIndexController',
      controllerAs: 'register'
    })

      .state('register.company', {
        url: '/company',
        templateUrl: 'app/view/register/company.html',
        controller: 'RegisterCompanyController',
        controllerAs: 'registerCompany',
        ncyBreadcrumb: {
          label: 'Cadastro de empresa'
        }
      })

      .state('register.branch', {
        url: '/branch',
        templateUrl: 'app/view/register/branch.html',
        controller: 'RegisterBranchController',
        controllerAs: 'registerBranch',
        ncyBreadcrumb: {
          label: 'Cadastro de filial'
        }
      })

      .state('register.additional', {
        url: '/additional',
        templateUrl: 'app/view/register/additional.html',
        controller: 'RegisterAdditionalController',
        controllerAs: 'registerAdditional',
        ncyBreadcrumb: {
          label: 'Dados adicionais'
        }
      })

      .state('register.personal', {
        url: '/personal',
        templateUrl: 'app/view/register/personal.html',
        controller: 'RegisterPersonalController',
        controllerAs: 'registerPersonal',
        ncyBreadcrumb: {
          label: 'Dados do usuário'
        }
      })

      .state('register.employe', {
        url: '/employee',
        templateUrl: 'app/view/register/employe.html',
        controller: 'RegisterEmployeController',
        controllerAs: 'registerEmploye',
        ncyBreadcrumb: {
          label: 'Cadastro de funcionário'
        }
      })

    .state('plans', {
      url: '/plans',
      templateUrl: 'app/view/plans/index.html',
      controller: 'PlansIndexController',
      controllerAs: 'plans',
      ncyBreadcrumb: {
        label: 'Planos'
      }
    })

    .state('payment', {
      url: '/payment',
      templateUrl: 'app/view/payment/index.html',
      controller: 'PaymentIndexController',
      controllerAs: 'payment',
      ncyBreadcrumb: {
        label: 'Pagamento'
      }
    })

    .state('equip', {
      url: '/equip',
      templateUrl: 'app/view/equip/index.html',
      controller: 'EquipIndexController',
      controllerAs: 'equip',
      ncyBreadcrumb: {
        label: 'Equipe'
      }
    })

    .state('control', {
      abstract: true,
      url: '/control',
      templateUrl: 'app/view/control/index.html',
      controller: 'ControlIndexController',
      controllerAs: 'control'
    })

      .state('control.refund', {
        url: '/refund',
        parent: 'control',
        templateUrl: 'app/view/control/refund.html',
        controller: 'ControlRefundController',
        controllerAs: 'refund',
        ncyBreadcrumb: {
          label: 'Reembolso'
        }
      })

    ;

  $urlRouterProvider.otherwise('/');
}
