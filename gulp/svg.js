// var gulp = require('gulp');
// var svgstore = require('gulp-svgstore');
// var inject = require('gulp-inject');
//
// gulp.task('svgstore', function () {
//     var svgs = gulp
//         .src('src/assets/svgs/*.svg')
//         .pipe(svgstore({ inlineSvg: true }));
//
//     function fileContents (filePath, file) {
//         return file.contents.toString();
//     }
//
//     return gulp
//         .src('src/index.html')
//         .pipe(inject(svgs, { transform: fileContents }))
//         .pipe(gulp.dest('src/assets/svgs/dist'));
// });

var gulp = require('gulp');
var rename = require('gulp-rename');
var path = require('path');
var svgstore = require('gulp-svgstore');
// var svgmin = require('gulp-svgmin');

gulp.task('svgstore', function () {
    return gulp
        .src('src/assets/svgs/*.svg')

        .pipe(svgstore())
        .pipe(rename(function(filepath) {
            filepath.basename = 'icons'
        }))
        .pipe(gulp.dest('src/assets/svgs/dist'));
});
