# 88

## Instalado depêndencias
Para iniciar o projeto execute:

```
npm run start
```

Instala as dependencias do package.json, bower.json e inicia o sprite em SVG.

## Iniciando server

```
npm run serve
```

## Build

```
npm run build
```

> Gerador Gulp Angular [Link](https://github.com/Swiip/generator-gulp-angular)
